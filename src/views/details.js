import React from  "react";
import "./../assets/css/details.css";



class Details  extends React.Component{


   render(){


     return(

       <div className="details">

       <div className="card_d">

       <div className="top_d">
       <h3 className="title_d">{this.props.title}</h3><p className="date_d"> {this.props.date}</p>
       </div>

       <div className="down_d">
       <img id="poster" src={"https://image.tmdb.org/t/p/w500/"+this.props.poster}/>
       <p id="sinopse">Sinopse</p>

       <p className='text_d'>{this.props.sinopse}  </p>
       <p id="information">Informações</p>
       <ul className="list">
       <p  className="listp">Situação</p>
       <p  className="list">{this.props.status}</p>
       </ul>
       <ul className="list">
       <p  className="listp">Idioma</p>
       <p  className="list">{this.props.language}</p>
       </ul>
       <ul className="list">
       <p  className="listp">Duração</p>
       <p  className="list">{this.props.runtime}</p>
       </ul>
       <ul className="list">
       <p  className="listp">Orçamento</p>
       <p  className="list">{this.props.budget}</p>
       </ul>
       <ul className="list">
       <p  className="listp">Receita</p>
       <p  className="list">{this.props.revenue}</p>
       </ul>
       <ul className="list">
       <p  className="listp">Lucro</p>
       <p  className="list">{this.props.profit}</p>
       </ul>



    <div className="per">  <p className="percent_d">{this.props.percent}</p> </div>

       </div>
       <iframe id="player" type="text/html"
         src={this.props.video}
         frameborder="0"></iframe>


       </div>
       </div>
     );
   };

};

export default Details;
