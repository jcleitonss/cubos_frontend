import React from "react";
import "./../assets/css/lista.css";
import Movies from "./movies.js";
import Details from "./details.js";

  class Lista extends React.Component {
////////////////////////////////////////////////////////////////
  constructor(props){
  super(props);
  this.state = {
  input:"",
  render:{
  movie0:false,
  movie1:false,
  movie2:false,
  movie3:false
  },
  movie0:"",
  movie1:"",
  movie2:"",
  movie3:"",
  movie4:"",
  page:"",
  button:{
   B1:"",
   B2:"",
   B3:"",
   B4:"",
   },
   details:"",
   language:"",
    trailer:"",
   lucro:"",
   genres:"",

  };
  };
/////////////////////////////////////////////////////////////


    Request(page) {
    //Faz uma requisição ao servidor com os dados digitados no input
    this.setState({button:{B1:false,B2:false,B3:false,B4:false}});
    var url ="https://api.themoviedb.org/3/genre/movie/list?api_key=253f44f7688b34cf92ef7cae16379328&language=pt-Br";
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, false);
    xhttp.send();
    var genres = JSON.parse(xhttp.responseText);


    this.setState({genres:genres})

    var busca= document.getElementById('input').value;
    if (busca != "") {
    var busca = busca.replace(" ","+");
    var url = "https://api.themoviedb.org/3/search/movie?api_key={253f44f7688b34cf92ef7cae16379328}&&language=pt-BR&query="+busca;
    for (var i = 0; i < 19;i++) {
      if (genres.genres[i].name.toLowerCase()==busca) {
        url= "http://api.themoviedb.org/3/discover/movie?api_key=253f44f7688b34cf92ef7cae16379328&language=pt-bR&with_genres="+genres.genres[i].id;
      };};
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, false);
    xhttp.send();
    var obj = JSON.parse(xhttp.responseText);
    //A requisição retorna do servidor e passamos ela pra o formato JSON para manipular as informações como um objeto

    this.List(obj,page);
    return 0;
    };

    };
/////////////////////////////////////////////////////////////
    List(object,page) {


      var obj=object;
      if (obj==0) {
        this.setState({render:{movie0:false,movie1:false,movie2:false,movie3:false,movie4:false}});
        this.setState({page:{B1:false,B2:false,B3:false,B4:false,}});
        return 0  ;
      };

      var i=0;
      var results=20;
      switch (page) {
        case 1:i=0;
        break;
        case 2:i=4;
        break;
        case 3:i=9;
        break;
        case 4:i=14;
        break;
      }
               ////// Resolução do bug para quando digita uma string muito longa
      if (obj.status_code ==25) {
       return 0;
      };

      if (obj.total_results<20) {results=obj.total_results;};
      this.Pages(results,page);


      switch (obj.total_results) {
      case 0:  this.setState({render:{movie0:false,movie1:false,movie2:false,movie3:false,movie4:false}});
      return 0;
      break;
      case 1:  this.setState({render:{movie0:true,movie1:false,movie2:false,movie3:false,movie4:false}});
      break;
      case 2:  this.setState({render:{movie0:true,movie1:true,movie2:false,movie3:false,movie4:false}});
      break;
      case 3:  this.setState({render:{movie0:true,movie1:true,movie2:true,movie3:false,movie4:false}});
      break;
      case 4:  this.setState({render:{movie0:true,movie1:true,movie2:true,movie3:true,movie4:false}});
      break;
      default:this.setState({render:{movie0:true,movie1:true,movie2:true,movie3:true,movie4:true}});
      };






    this.Imprime(object,results,i)
    return 0;



    };
     ////////

    Imprime(object,results,i){
    var obj=object;

     var im=i;
    var vetor=new Array();
    while (i< results) {
    vetor[i]={title:obj.results[i].title,date:obj.results[i].release_date,id:obj.results[i].id,percent:obj.results[i].vote_average*10+"%",overview:obj.results[i].overview,image:obj.results[i].poster_path};
    i++;
    };
    ///////////


    this.setState({movie0:vetor[im]});
    im++;
    this.setState({movie1:vetor[im]});
    im++;
    this.setState({movie2:vetor[im]});
    im++;
    this.setState({movie3:vetor[im]});
    im++;
    this.setState({movie4:vetor[im]});









  return 0;
  };

    ///////////////////////////////////////////////////////////
    Details(id){



      var url ="https://api.themoviedb.org/3/movie/"+id+"/videos?api_key=253f44f7688b34cf92ef7cae16379328&language=pt-bR";
      var xhttp = new XMLHttpRequest();
      xhttp.open("GET", url, false);
      xhttp.send();
      var video = JSON.parse(xhttp.responseText);
      if (video.results[0]==null) {
      var trailer="";
      }
      else {
        var trailer="https://www.youtube.com/embed/"+video.results[0].key;
      };






      var url ="https://api.themoviedb.org/3/movie/"+id+"?api_key=253f44f7688b34cf92ef7cae16379328&language=pt-Br";
      var xhttp = new XMLHttpRequest();
      xhttp.open("GET", url, false);
      xhttp.send();
      var details = JSON.parse(xhttp.responseText);
      this.setState({details:details});
      this.setState({trailer:trailer});
      this.setState({genres:details.genres})
      this.setState({lucro:details.revenue-details.budget});


      this.List(0,0);

      return 0;
      };
     //////////////////////////////////////////////////////////
     Pages(results,page){
     var resto=results % 5;
     var pages=(results-resto)/5;

       switch (pages) {
       case 0:this.setState({page:{B1:false,B2:false,B3:false,B4:false,}});
       break;
       case 1:this.setState({page:{B1:true,B2:false,B3:false,B4:false,}});
       break;
       case 2:this.setState({page:{B1:true,B2:true,B3:false,B4:false,}});
       break;
       case 3:this.setState({page:{B1:true,B2:true,B3:true,B4:false,}});
       break;
       default:this.setState({page:{B1:true,B2:true,B3:true,B4:true,}});
       };


         switch (page) {
         case 0: this.setState({button:{B1:"active",B2:false,B3:false,B4:false}});
         break
         case 1: this.setState({button:{B1:"active",B2:false,B3:false,B4:false}});
         break;
         case 2:this.setState({button:{B1:false,B2:"active",B3:false,B4:false}});
         break;
         case 3:this.setState({button:{B1:false,B2:false,B3:"active",B4:false}});
         break;
         case 4:this.setState({button:{B1:false,B2:false,B3:false,B4:"active"}});
         break;
         };






    return 0;
     };

    //////////////////////////////////////

   render(){
    return(

    <div>

    <div className="header" >
    <h1 className="movie">Movies</h1>

    </div >


    <div>

    <input placeholder="Busque filmes por nome, ano ou gênero..."  type="text" id="input" onChange={this.Request.bind(this)} />
    </div>
    <div id="movie" >
    {this.state.details.id &&<Details percent={this.state.details.vote_average*10+"%"} title={this.state.details.title} date={this.state.details.release_date} poster={this.state.details.poster_path} sinopse={this.state.details.overview} status={this.state.details.status} language={this.state.details.original_language} runtime={parseInt(this.state.details.runtime/60)+"H"+this.state.details.runtime%60+"min"} budget={"$"+this.state.details.budget.toLocaleString()} revenue={"$"+this.state.details.revenue.toLocaleString()} profit={"$"+this.state.lucro.toLocaleString()} video={this.state.trailer} genres={this.state.genres[0].name} />}

    {this.state.render.movie0 &&< Movies id={this.state.movie0.id} title={this.state.movie0.title} overview={this.state.movie0.overview} date={this.state.movie0.date} percent={this.state.movie0.percent} image={this.state.movie0.image} function={()=>this.Details(this.state.movie0.id)}/>}
    <br/>
    <br/>
    <br/>
    {this.state.render.movie1 &&<Movies id={this.state.movie1.id} title={this.state.movie1.title} overview={this.state.movie1.overview} date={this.state.movie1.date} percent={this.state.movie1.percent} image={this.state.movie1.image} function={()=>this.Details(this.state.movie1.id)}/>}
    <br/>
    <br/>
    <br/>
    {this.state.render.movie2 &&< Movies id={this.state.movie2.id} title={this.state.movie2.title} overview={this.state.movie2.overview} date={this.state.movie2.date} percent={this.state.movie2.percent} image={this.state.movie2.image} function={()=>this.Details(this.state.movie2.id)}/>}
    <br/>
    <br/>
    <br/>
    {this.state.render.movie3 &&< Movies id={this.state.movie3.id} title={this.state.movie3.title} overview={this.state.movie3.overview} date={this.state.movie3.date} percent={this.state.movie3.percent} image={this.state.movie3.image} function={()=>this.Details(this.state.movie3.id)}/>}
    <br/>
    <br/>
    <br/>
    {this.state.render.movie4 &&< Movies id={this.state.movie4.id}  title={this.state.movie4.title} overview={this.state.movie4.overview} date={this.state.movie4.date} percent={this.state.movie4.percent} image={this.state.movie4.image} function={()=>this.Details(this.state.movie4.id)}/>}
    </div>
    <br/>
    <br/>
    <br/>
    <div className="page">
    {this.state.page.B1&&<input type="button" value="1" className="button" id={this.state.button.B1} onClick={() => this.Request(1)}/>}
    {this.state.page.B2&&<input type="button" value="2" className="button" id={this.state.button.B2} onClick={() => this.Request(2)}/>}
    {this.state.page.B3&&<input type="button" value="3" className="button" id={this.state.button.B3} onClick={() => this.Request(3)}/>}
    {this.state.page.B4&&<input type="button" value="4" className="button" id={this.state.button.B4} onClick={() => this.Request(4)}/>}
      </div>

    <br/>
    <br/>
    <br/>

    </div>

  );};
};

export default Lista;
