import React from  "react";
import "./../assets/css/movie.css"
var logo="https://image.tmdb.org/t/p/w500/mt0ELw83HIRgaKaDzhsDcIi0RnW.jpg";
class Movies extends React.Component{



   render(){


     return(

       <div id="card" className="card">

       <div className="top">
       <div className="bol"> <p className="percent">{this.props.percent}</p> </div>
       <h3 className="title">{this.props.title}</h3>
       </div>

       <div className="down">
       <p className="date">{this.props.date}</p>
       <p className='text'> {this.props.overview} </p>
       </div>

       <img className="image" src={"https://image.tmdb.org/t/p/w500/"+this.props.image}/>
        <input type="button" className="divlink" onClick={this.props.function}/>

       </div>
     );
   };

};

export default Movies;
